import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap, filter } from 'rxjs/operators';
import { TovickbrowserService } from 'src/app/services/tovickBrowser/tovickbrowser.service';
import { environment } from '../../../environments/environment';
import { FormControl } from '@angular/forms';
import { ThrowStmt } from '@angular/compiler';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FauthService } from 'src/app/services/fauth/fauth.service';
import { Router } from '@angular/router';
import * as jsel from 'jsel';

@Component({
  selector: 'app-combo-products-list',
  templateUrl: './combo-products-list.component.html',
  styleUrls: ['./combo-products-list.component.scss']
})
export class ComboProductsListComponent implements OnInit, OnChanges {

  constructor(
    private route: ActivatedRoute,
    private _svc: TovickbrowserService,
    private _snackBar: MatSnackBar,
    private _fauth: FauthService,
    public router: Router,
  ) { }

  @Input() refcatId;
  @Input() catMaxPrice;
  @Input() offerTitle;
  @Output() prodSelectEvent = new EventEmitter();
  catId: any;
  objectKeys = Object.keys;
  categoryInfo: any;
  currentCat = false;
  products: any;
  allProducts: any;
  prodId: any;
  currentProd: any;
  image_path: string = environment.image_host + environment.image_subpath.product;
  sortField = 'popular';
  selectedType: any = false;
  filterControls: object = {};
  toApplyFilters = false;
  resetFlag = false;

  filters: object = { sort: 'popular', colors: '', material: '', setof: '' };
  isfiltered = false;
  isMoreProd = true;
  testcolor: any;
  flagarray = { material: [] };
  ngOnChanges(changes: import ('@angular/core').SimpleChanges): void {
    this.ngOnInit();
  }

  ngOnInit() {
    let catinfoobj;
    let TypeIdtoFetch;
    this._svc.fetchCategories().subscribe((res) => {
      catinfoobj = this.searchTypeInfo(this.refcatId);

      if (catinfoobj) {
        console.log('listing by typeID');
        this.catId = catinfoobj.category_id;
        TypeIdtoFetch = catinfoobj.id;

      } else {
        console.log('listing by catID');
        this.catId = this.refcatId;
        const types = this.getTypes(this.catId);
        TypeIdtoFetch = types[0].id;
      }

      const subs = this._svc.getProducts(this.catId);
      subs.subscribe((res) => {
        this.fetchCatInfo(true);
        this.currentCatData();
        // select first product type
        const types = this.getTypes(this.catId);
        // tslint:disable-next-line: no-string-literal
        this.products = res['result'];
        this.allProducts = this.products; // initial fetch hass all products without filters
        this.isMoreProd = true;
        console.log('products');
        console.log(this.products);
        this.changeProductTypeById(TypeIdtoFetch);
      });
    });



  }
  resetFilters() {
    this.resetFlag = true;
    const x = this.filterControls;
    this.filterControls = [];
    this.filters = { sort: 'popular', colors: '', material: '', setof: '' };
    this._svc.getProducts(this.catId).subscribe((res) => {
      this.filterControls = x;
      this.fetchCatInfo();
      // tslint:disable: no-string-literal
      this.products = res['result'];
      this.isfiltered = false;
      this.isMoreProd = true;

    });

  }
  fetchCatInfo(force?: boolean) {
    if (!this.categoryInfo || force) {
      this.categoryInfo = this._svc.getCatSiblings(this.catId);
      console.log('categoryInfo:');
      console.log(this.categoryInfo);
    }

  }
  searchTypeInfo(id) {
    // console.log('Categories:');
    // console.log(this._svc.categories);
    const catinfodom = jsel(this._svc.categories);
    console.log('dom select:');
    const tuplet = catinfodom.select('//types/*[@id="' + id + '"]');
    console.log(tuplet);
    return tuplet;
  }
  currentCatData() {
    this.fetchCatInfo(true);
    if (this.categoryInfo) {
      for (const categ of this.categoryInfo) {
        if (categ.id == this.catId) {
          this.currentCat = categ;
        }
      }
    }
  }
  setFilter(e, value, filterNom) {
    const Arr =
      (this.filters[filterNom] != ''
        && this.filters[filterNom] != undefined)
        ? this.filters[filterNom].split(',')
        : [];
    if (e.target.checked) {
      Arr.push(value);

    } else {
      for (const x in Arr) {
        if (Arr[x] == value) {
          Arr.splice(x);
        }
      }

    }
    this.filters[filterNom] = Arr.join(',');
    console.log('filter ' + filterNom + ': ' + this.filters[filterNom]);
    this.toApplyFilters = true;
  }
  inFilter(filterNom, value): boolean {
    const z_filter = this.filters[filterNom];
    const Arr =
      (z_filter != undefined &&
        z_filter != ''
        && z_filter[filterNom] != undefined)
        ? z_filter[filterNom].split(',')
        : [];
    for (const x in Arr) {
      if (Arr[x] == value) {
        return true;
      }
    }
    return false;
  }


  fetchProductFilters() {
    this._svc.getProductFilters(this.catId, this.selectedType).subscribe((res) => {
      const results = res;

      delete results['status'];

      this.filterControls = { ...this.filterControls, ...results };
    });

  }
  fetchFilteredProducts(offset?: number, limit?: number) {
    this.toApplyFilters = false;
    this.isfiltered = true;
    offset = !offset ? 0 : offset;
    limit = !limit ? 10 : limit;
    this._svc.getProducts(this.catId, 'filtered', this.filters, offset, limit).subscribe((res) => {
      console.log(res);


      if (offset == 0) {

        // tslint:disable: no-string-literal
        this.products = res['result'];
        // console.log(this.products);
      } else {

        if (!this.isMoreProd) {
          return null;
        }
        this.isMoreProd = false;
        if (res['result'] != null) {

          this.products = [...this.products, ...res['result']];
          this.isMoreProd = true;
        } else {
          this.isMoreProd = false;
        }

      }
    });
  }
  getTypes(catID) {
    if (this.categoryInfo != undefined) {
      for (const tuple of this.categoryInfo) {
        if (tuple.id == catID) {
          // tslint:disable-next-line: no-string-literal
          return tuple['types'];
        }
      }
    }
  }
  changeProductType() {
    // reset filters
    this.filters = { sort: this.sortField, typeid: this.selectedType };
    this.fetchProductFilters();
    this.fetchFilteredProducts();
  }
  changeProductTypeById(selTypeid) {

    this.selectedType = selTypeid;
    // reset filters
    this.filters = { sort: this.sortField, typeid: selTypeid };
    this.fetchProductFilters();
    this.fetchFilteredProducts();
  }
  openProdDetails(productobj) {
    console.log(productobj);
    this.currentProd = productobj;
    this.prodId = productobj.id;
  }
  closePrdoDetails() {
    this.prodId = false;
  }

  changeSortField() {
    // tslint:disable-next-line: no-string-literal
    this.filters['sort'] = this.sortField;
    this.fetchFilteredProducts();
  }

  addToWishList(productobj) {
    const uid = this.checkLogin();
    if (uid) {
      this._svc.addToWishlist(productobj.id, uid).subscribe((res) => {
        this._snackBar.open('Added To Wishlist', null, {
          duration: 2000,
        });
      });
    }
  }
  addToCart(productobj) {
    const uid = this.checkLogin();
    this.prodSelectEvent.emit(productobj);
    if (false) {
      this._svc.addToCart(productobj.product.id, productobj.quantity, productobj.priceid, uid).subscribe((res) => {
        this._snackBar.open('Added To Cart', null, {
          duration: 2000,
        });
      });
    }
  }
  addDefaultToCart(productobj) {
    const uid = this.checkLogin();
    if (false) {
      this._svc.addToCart(productobj.id, 1, productobj.prices[0].id, uid).subscribe((res) => {
        this._snackBar.open('Added To Cart', null, {
          duration: 2000,
        });
      });
    }
  }
  fetchMore() {
    if (!this.isMoreProd) {
      return null;
    }

    if (this.isfiltered) {
      this.fetchFilteredProducts(this.products.length + 1, 10);

    } else {
      this.isMoreProd = false;
      this._svc.getProducts(this.catId, 'items', false, this.products.length + 1, 10)
        .subscribe((resp) => {
          if (resp['result'] != null) {
            this.products = [...this.products, ...resp['result']];
            this.isMoreProd = true;
          } else {
            this.isMoreProd = false;
          }

        });

    }

  }
  loopable(stuff) {
    let truthy = stuff ? true : false;
    if (stuff == undefined) {
      truthy = false;
    }
    if (stuff == null) {
      truthy = false;
    }
    if (stuff == {}) {
      truthy = false;
    }
    if (stuff == []) {
      truthy = false;
    }
    if (truthy) {
      if (stuff.length == 0) {
        truthy = false;
      }
    }
    return truthy;
  }
  validProduct(product) {
    let truthy = product ? true : false;
    if (product == undefined) {
      truthy = false;
    }
    if (product == null) {
      truthy = false;
    }
    if (product == {}) {
      truthy = false;
    }
    if (product == []) {
      truthy = false;
    }
    if (product.prices[0].offer_price > this.catMaxPrice) {
      truthy = false;

    }
    if (truthy) {
      if (product.length == 0) {
        truthy = false;
      }
    }

    return truthy;
  }
  validPrice(price) {
    let truthy = price ? true : false;
    if (price == undefined) {
      truthy = false;
    }
    if (price == null) {
      truthy = false;
    }
    if (price == {}) {
      truthy = false;
    }
    if (price == []) {
      truthy = false;
    }
    if (truthy) {
      if (price.length == 0) {
        truthy = false;
      }
    }
    return truthy;
  }
  checkLogin() {
    if (!this._fauth.getCurrentUID()) {
      this.router.navigate(['/user/login']);
      return false;
    } else {
      return true;
    }

  }

}
