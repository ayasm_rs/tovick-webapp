import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComboProductsListComponent } from './combo-products-list.component';

describe('ComboProductsListComponent', () => {
  let component: ComboProductsListComponent;
  let fixture: ComponentFixture<ComboProductsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComboProductsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComboProductsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
