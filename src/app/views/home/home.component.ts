import { Component, OnInit } from '@angular/core';
import {TovickbrowserService} from 'src/app/services/tovickBrowser/tovickbrowser.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  bannerSlides;
  comboSlides;


  constructor(private svc: TovickbrowserService) { }

  ngOnInit() {
    this.svc.getHomeData().subscribe((res) =>{
      this.bannerSlides = res['slider'];
      this.comboSlides = res["combo_offers"];
    });
  }

}
