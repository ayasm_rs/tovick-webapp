import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  url = environment.service_api + 'Home/loadInfo';

  constructor(private http: HttpClient) {
   }
   getData() {
     let options = {
       headers: new HttpHeaders({
        'Content-Type':  'application/x-www-form-urlencoded',
         'x-api-key': 'TOVICK1007'
     })
    };
     return this.http.get(this.url, options);
   }
}
