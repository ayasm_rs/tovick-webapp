import { Component, OnInit, SecurityContext } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
import { TovickbrowserService } from 'src/app/services/tovickBrowser/tovickbrowser.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  categoryInfo: any;
  image_path = environment.image_host + environment.image_subpath.category;
  target: object;

  constructor(private router: Router, private _svc: TovickbrowserService, private _sanitizer: DomSanitizer) { }

  ngOnInit() {
    this._svc.fetchCategories().subscribe((res) => {
      // tslint:disable-next-line: no-string-literal
      this.categoryInfo = res['result'];
      this.set_display_target(this.categoryInfo);
    });
  }
  get_bg_style(str: string) {
    const safeUrl = this._sanitizer.sanitize(SecurityContext.URL, str);
    str = this._sanitizer.sanitize(SecurityContext.STYLE, 'background-image: url(\'' + safeUrl + '\');');
    this._sanitizer.bypassSecurityTrustStyle(str);
    return str;
  }
  set_display_target(category: any) {
    this.target = category;
  }

  showCategoryProds(id: number) {
    this.router.navigateByUrl('/products/' + id);
  }

}
