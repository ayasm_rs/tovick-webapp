import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { environment } from '../../../environments/environment';
import { FormControl } from '@angular/forms';
import { TovickbrowserService } from 'src/app/services/tovickBrowser/tovickbrowser.service';

@Component({
  selector: 'app-combo-product-details',
  templateUrl: './combo-product-details.component.html',
  styleUrls: ['./combo-product-details.component.scss']
})
export class ComboProductDetailsComponent implements OnInit {
  @Input() product: any;

  @Output() wishlistEvent = new EventEmitter();
  @Output() cartEvent = new EventEmitter();
  selectedPrice: any = false;
  qty = 1;
  price_index = new FormControl();
  thumb_index: number;
  filters: any = false;
  swiperconfig: SwiperConfigInterface = {
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  };
  c_thumb: string;
  img_root = environment.image_host + environment.image_subpath.product;


  constructor(private _sanitizer: DomSanitizer, private _svc: TovickbrowserService) { }

  ngOnInit() {
    this.thumb_index = 1;
    if (this.product != undefined) {
      this.selectedPrice = this.product.prices[0];
      this._svc.getProductFilters(this.product.category_id, this.product.sub_cat_id).subscribe((res) => {
        this.filters = res;

      });
    }
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    console.log(this.product);
    if (this.product != undefined && this.product.prices != undefined) {
      this.selectedPrice = this.product.prices[0];
    }
  }
  sanitizehtm(html) {
    this._sanitizer.bypassSecurityTrustHtml(html);
    return html;
  }
  incQty() {
    if (this.qty < 5) {
      this.qty++;
    }
  }
  decQty() {
    if (this.qty > 1) {
      this.qty--;
    }
  }
  select_price() {
    this.selectedPrice = this.product.prices[this.price_index.value];
  }
  emitFavorite() {
    const favorite = {};
    // tslint:disable-next-line: no-string-literal
    favorite['product'] = this.product;
    // tslint:disable-next-line: no-string-literal
    favorite['priceid'] = this.selectedPrice['id'];
    this.wishlistEvent.emit(favorite);

  }
  emitCart() {
    const cartitem = {};
    // tslint:disable-next-line: no-string-literal
    cartitem['product'] = this.product;
    // tslint:disable-next-line: no-string-literal
    cartitem['priceid'] = this.selectedPrice['id'];
    // tslint:disable-next-line: no-string-literal
    cartitem['quantity'] = this.qty;
    this.cartEvent.emit(cartitem);

  }
  hasprice(): boolean {
    return this.product.prices && this.selectedPrice ? true : false;

  }
  thumb(numero) {
    this.thumb_index = numero;
  }
  currentThumb() {
    let z_thumb: string;
    switch (this.thumb_index) {
      case 1:
        z_thumb = this.product.image_one;
        break;
      case 2:
        z_thumb = this.product.image_one;
        break;
      case 3:
        z_thumb = this.product.image_one;
        break;
      case 4:
        z_thumb = this.product.image_one;
        break;
      default:
        z_thumb = this.product.image_one;
    }

    return this.img_root + this.product.id + '/' + z_thumb;
  }


}
