import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComboProductDetailsComponent } from './combo-product-details.component';

describe('ComboProductDetailsComponent', () => {
  let component: ComboProductDetailsComponent;
  let fixture: ComponentFixture<ComboProductDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComboProductDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComboProductDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
