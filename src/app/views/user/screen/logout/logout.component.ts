import { Component, OnInit } from '@angular/core';
// services
import { FauthService } from 'src/app/services/fauth/fauth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(
    private _fauth: FauthService,
    public route: Router,
  ) { }

  ngOnInit() {
    this._fauth.signout();
    this.route.navigate(['/user/login']);
  }

}
