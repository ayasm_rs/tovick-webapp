import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';

// services
import { FauthService } from 'src/app/services/fauth/fauth.service';
import { WindowService } from 'src/app/services/window/window.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {
  inputForm: FormGroup;
  otpVerificationForm: FormGroup;
  windowRef: any;
  confirmation: any;

  formHeading = 'Login';
  formMsg = 'Get access to your Orders and Wishlist';
  stage = 'input';
  error = false;
  processing = false;

  constructor(
    private _fauth: FauthService,
    private _win: WindowService,
    public route: Router,
  ) { }

  ngOnInit() {
    this.checkLogin();

    this.windowRef = this._win.windowRef;
    // Initialize login form
    this.inputForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      mobile: new FormControl('', Validators.compose([Validators.required/*, Validators.pattern('^[2-9]{2}[0-9]{8}$')*/]))
    });

    // Initialize OTP entry form
    this.otpVerificationForm = new FormGroup({
      otp: new FormControl('', Validators.required)
    });
  }
  ngAfterViewInit(): void {
    if (this._fauth.getCurrentUID()) {
      this.route.navigate(['/wishlist']);
    }
  }
  checkLogin() {
    if (this._fauth.getCurrentUID()) {
      this.route.navigate(['/wishlist']);
      return true;
    } else {
      return false;
    }
  }
  setCaptcha(switcher = true, winRef = this.windowRef) {
    const truthy: boolean = winRef.recaptchaVerifier == undefined ? true : false;
    if (switcher) {
      // Initialize recaptcha on container
      const myauth = this._fauth.getAuthInstance();
      winRef.recaptchaVerifier = new myauth.RecaptchaVerifier('recaptcha-container', { size: 'invisible' });
      winRef.recaptchaVerifier.render();
    } else {
      // reset captcha
      console.log('Resetting the Captcha\n');
      winRef.recaptchaVerifier.render().then((widgetId) => {
        winRef.grecaptcha.reset(widgetId);
      });
    }
  }
  sendCode() {
    if (this.checkLogin()) {
      return null;
    }
    this.processing = true;
    this.setCaptcha(!this.error, this.windowRef);
    // call to mobile API here
    const data = this.inputForm.value;
    this._fauth.send_OTP('+91' + data.mobile, this.windowRef).then(
      result => {
        console.log('SMS Sent:');
        console.log(result);
        this.formHeading = 'OTP';
        this.formMsg = 'Enter the verification code sent to your phone to continue';
        this.stage = 'otp';
        this.confirmation = result;
        this.processing = false;
      })
      .catch(
        error => {
          console.log(error);
          this.formHeading = 'ERROR';
          this.formMsg = 'Try again.';
          this.error = true;
          // reset captcha
          this.setCaptcha(false, this.windowRef);
          this.processing = false;
        });
  }
  verify() {
    this.processing = true;
    const otp = this.otpVerificationForm.value.otp;
    // verify otp
    this.confirmation.confirm(otp).then(result => {
      alert('OTP Verified.');
      // localStorage.setItem('phone', JSON.stringify(this.numVerificationForm.value.mnumber));
      this.route.navigate(['/']);
      this.processing = false;
    })
      .catch(error => {
        console.log(error.message);
        this.error = true;
        // show signin form
        this.formHeading = 'Error';
        this.formMsg = 'Invalid OTP. Try again.';
        this.stage = 'input';
        this.processing = false;

      });

  }

}
