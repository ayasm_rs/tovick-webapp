import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { LoginComponent } from './screen/login/login.component';
import {
  MatProgressBarModule,
  MatButtonModule,
  MatInputModule,
  MatCardModule,
  MatCheckboxModule,
  MatIconModule,
  MatProgressSpinnerModule
 } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {AngularFireAuthModule} from '@angular/fire/auth';
//services
import {FauthService} from '../../services/fauth/fauth.service';
import {WindowService} from '../../services/window/window.service';
import { LogoutComponent } from './screen/logout/logout.component';

@NgModule({
  declarations: [LoginComponent, LogoutComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    MatCardModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireAuthModule
  ],
  providers:[FauthService,WindowService]
})
export class UserModule { }
