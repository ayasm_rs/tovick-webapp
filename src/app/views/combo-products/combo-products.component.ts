import { Component, OnInit } from '@angular/core';
import { TovickbrowserService } from 'src/app/services/tovickBrowser/tovickbrowser.service';
import { FauthService } from 'src/app/services/fauth/fauth.service';
import { Router, ActivatedRoute } from '@angular/router';

import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-combo-products',
  templateUrl: './combo-products.component.html',
  styleUrls: ['./combo-products.component.scss']
})
export class ComboProductsComponent implements OnInit {
  combos;
  comboID: number;
  currentCombo;
  imagepath = environment.image_host + 'combo/';
  productList = [false, false, false];
  currentIndex = 0;
  loadProdList = false;
  currentCat = {
    max_price: '',
    cat_id: '',
    offer_title: '',
  };


  constructor(
    private _svc: TovickbrowserService,
    private _fauth: FauthService,
    private route: ActivatedRoute,
    private _sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    const rParams = this.route.paramMap;
    rParams.subscribe((out) => {
      this.comboID = +out.get('cid');
      this._svc.getHomeData().subscribe((res) => {
        // tslint:disable-next-line: no-string-literal
        this.combos = res['combo_offers'];
        for (const combo of this.combos) {
          if (combo.offer.id == this.comboID) {
            this.currentCombo = combo.offer;
            console.log('Combo Info');
            console.log(this.currentCombo);
          }
        }
      });
    });


  }
  imgStyle(filename) {
    const imgURL = this.imagepath + filename;
    const styleRule = 'background-image: url(\'' + imgURL + '\');';
    this._sanitizer.bypassSecurityTrustStyle(styleRule);
    return styleRule;

  }
  AddProdSelect(ev) {
    console.log(ev);
    if (ev.product.category_id == this.currentCat.cat_id || ev.product.sub_cat_id == this.currentCat.cat_id) {
      this.productList[this.currentIndex] = ev;
      this.loadProdList = false;

    } else {
      alert('Selected Product cannot be added to combo.');
      this.productList[this.currentIndex] = false;
      this.prepProdSelection(this.currentIndex);
    }

  }
  prepProdSelection(index) {
    this.currentIndex = index;
    switch (index) {
      case 0:
        this.currentCat.cat_id = this.currentCombo.offer_cat_one;
        this.currentCat.max_price = this.currentCombo.offer_one_max_price;
        this.currentCat.offer_title = this.currentCombo.offer_one_name;
        break;
      case 1:
        this.currentCat.cat_id = this.currentCombo.offer_cat_two;
        this.currentCat.max_price = this.currentCombo.offer_two_max_price;
        this.currentCat.offer_title = this.currentCombo.offer_two_name;
        break;
      case 2:
        this.currentCat.cat_id = this.currentCombo.offer_cat_three;
        this.currentCat.max_price = this.currentCombo.offer_three_max_price;
        this.currentCat.offer_title = this.currentCombo.offer_three_name;
        break;
      default:
        break;
    }


    this.loadProdList = true;

  }
  AddToCart() {
    const uid = this._fauth.getCurrentUID();
    if (uid) {
      const catCodeMap = ['first', 'second', 'third', 'fourth', 'fifth', 'sixth'];
      const idList = [];
      const map = [];



      for (const x in this.productList) {
        if (this.productList[x]) {
          idList.push(this.productList[x]['product'].id);
        }
      }

      for (const key in idList) {
        if (idList[key]) {
          map.push({ cat_code: catCodeMap[key], product_id: idList[key] });
        }
      }

      this._svc.addComboToCart(uid, this.comboID, idList, this.currentCombo['offer_price'], map).subscribe(
        (res) => {
          alert('added to cart.');
        }
      );



    } else {
      alert('Please Login to continue.');

    }


  }



}
