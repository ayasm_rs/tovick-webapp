import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { FormGroup, FormControl } from '@angular/forms';
import { Router} from '@angular/router';
import { TovickbrowserService } from 'src/app/services/tovickBrowser/tovickbrowser.service';
import { FauthService } from 'src/app/services/fauth/fauth.service';
import { TovickuserService } from 'src/app/services/tovickuser.service';



@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  firstFormGroup = new FormGroup({
    cart: new FormControl('')
  });

  items: any;
  combos: any;
  image_path: string = environment.image_host + environment.image_subpath.product;
  addressForm = new FormGroup({
    user_id: new FormControl(''),
    lng: new FormControl(''),
    lat: new FormControl(''),
    landmark: new FormControl(''),
    address: new FormControl(''),

  });
  userAddrs: any;

  constructor(
    private _svc: TovickbrowserService,
    private _user: TovickuserService,
    private _fauth: FauthService,
    public route: Router
    ) { }

  ngOnInit() {
    if (this._fauth.getCurrentUID()) {
      this.getCartItems();
      this.getUsrAddrs();
    } else {
      this.route.navigate(['/user/login']);
    }
  }
  getCartItems() {
    this._svc.fetchUserCart(this._fauth.getCurrentUID()).subscribe((res) => {
      // tslint:disable-next-line: no-string-literal
      this.items = res['result'];
      // tslint:disable-next-line: no-string-literal
      this.combos = res['resultCombo'];
    });

  }
  removeFrmCart(cartid) {
    this._svc.removeFromCart(cartid).subscribe((res) => {
      this.getCartItems();
    });
  }
  removeComboFrmCart(cartid) {
    this._svc.removeComboFromCart(cartid).subscribe((res) => {
      this.getCartItems();
    });
  }
  getUnitPrice(item): number {
    const prices = item.productInfo.prices;
    let unitprice: number;

    for (const price of prices) {
      if (price.id == item.price_id) {
        unitprice = price.offer_price;
      }

    }
    return unitprice;
  }

  getSubtotal(item): number {

    return item.quantity * this.getUnitPrice(item);
  }

  cartTotal() {
    let total = 0;
    if (this.validCart(this.items)) {
      for (const item of this.items) {

        total += this.getSubtotal(item);

      }
    }
    if (this.validCart(this.combos)) {
      for (const combo of this.combos) {
        total += +combo.cartinfo.price;
      }

    }

    return total;
  }
  getUsrAddrs() {
    this._user.fetchAddresses()
      .subscribe((res) => {
        // tslint:disable-next-line: no-string-literal
        this.userAddrs = res['result'];

      });

  }
  validCart(coll_obj) {
    return (coll_obj != undefined || coll_obj != null) ? true : false;
  }
  count() {
    let count = 0;
    let combo_count = this.validCart(this.combos) ? this.combos.length : 0;
    let item_count = this.validCart(this.items) ? this.items.length : 0;
    count = combo_count + item_count;
    return count;

  }

}
