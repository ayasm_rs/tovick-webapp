import { Component, OnInit, HostListener, HostBinding } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap, filter } from 'rxjs/operators';
import { TovickbrowserService } from 'src/app/services/tovickBrowser/tovickbrowser.service';
import { TovickuserService } from 'src/app/services/tovickuser.service';
import { environment } from '../../../environments/environment';
import { FormControl } from '@angular/forms';

import {
  trigger,
  state,
  style,
  animate,
  transition,
  animateChild,
} from '@angular/animations';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FauthService } from 'src/app/services/fauth/fauth.service';
import { Router } from '@angular/router';
import { LoadingModalComponent } from 'src/app/common/loading-modal/loading-modal.component';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
  animations: [
    trigger('wishlisting', [
      state('yes', style({
        opacity: 1,
        color: 'red'
      })),
      state('transit', style({
        opacity: 0.2,
        color: 'white',
      })),
      state('no', style({
        color: '#8b8b8b',
        fontSize: '16px',
        opacity: 1,
      })),
      transition('transit => yes,transit => no', [
        animate('2s'), animateChild()
      ]),
    ]),
  ]
})
export class ProductListComponent implements OnInit {
  objectKeys = Object.keys;
  categoryID: number;
  categoryInfo: any;
  currentCat = false;
  products: any;
  allProducts: any;
  prodId: any;
  currentProd: any;
  image_path: string = environment.image_host + environment.image_subpath.product;
  sortField = 'popular';
  selectedType: any = false;
  filterControls: object = {};
  toApplyFilters = false;
  resetFlag = false;

  filters: object = { sort: 'popular', colors: '', material: '', setof: '' };
  isfiltered = false;
  isMoreProd = true;
  testcolor: any;
  flagarray = { material: [] };
  loadingDialog: any = false;

  wishlisted = 'no';
  wishlistFlags = [];
  userWishlist: any = false;



  constructor(
    private route: ActivatedRoute,
    private _svc: TovickbrowserService,
    private _snackBar: MatSnackBar,
    private _fauth: FauthService,
    private _user: TovickuserService,
    public router: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this._svc.fetchCategories();
    this.doLoading();
    this.refreshWishlist();


    const subs = this.route.paramMap.pipe(
      switchMap(params => {
        // (+) before `params.get()` turns the string into a number
        this.categoryID = +params.get('cid');
        this.prodId = +params.get('pid');
        return this._svc.getProducts(this.categoryID);
      })
    );
    subs.subscribe((res) => {

      this.fetchCatInfo(true);
      this.currentCatData();
      // select first product type
      const types = this.getTypes(this.categoryID);
      // tslint:disable-next-line: no-string-literal
      this.products = res['result'];
      this.allProducts = this.products; // initial fetch hass all products without filters
      this.isMoreProd = true;
      const len = this.products ? this.products.length : 0;
      if (len <= 9) {
        this.isMoreProd = false;
      }

      if (this.prodId) {
        const myProd = this._svc.getStoredProduct(this.prodId);
        myProd.then((res) => {
          console.log(res);
          if (res.length > 0) {
            this.stopLoading();
            this.currentProd = res[0];
          } else {
            for (const product of this.allProducts) {
              // tslint:disable-next-line: triple-equals
              if (product.id == this.prodId) {
                console.log('Product Found');
                this.stopLoading();
                this.currentProd = product;
              }
            }

          }

        });

      } else {
        this.stopLoading();
        if (types != undefined) {
          const firstType = types[0];
          this.changeProductTypeById(firstType.id);
        }
      }
    });
  }
  resetFilters() {
    this.resetFlag = true;
    const x = this.filterControls;
    this.filterControls = [];
    this.filters = { sort: 'popular', colors: '', material: '', setof: '' };
    this._svc.getProducts(this.categoryID).subscribe((res) => {
      this.filterControls = x;
      this.fetchCatInfo();
      // tslint:disable: no-string-literal
      this.products = res['result'];
      this.isfiltered = false;
      this.isMoreProd = true;

    });

  }



  /**
   * Fetch category information on page
   * @param force flag to indicate whether update is to be forced
   */
  fetchCatInfo(force?: boolean) {
    if (!this.categoryInfo || force) {
      this.categoryInfo = this._svc.getCatSiblings(this.categoryID);
    }
  }
  currentCatData() {
    for (const categ of this.categoryInfo) {
      if (categ.id == this.categoryID) {
        this.currentCat = categ;

      }

    }
  }
  setFilter(e, value, filterNom) {
    const Arr =
      (this.filters[filterNom] != ''
        && this.filters[filterNom] != undefined)
        ? this.filters[filterNom].split(',')
        : [];
    if (e.target.checked) {
      Arr.push(value);

    } else {
      for (const x in Arr) {
        if (Arr[x] == value) {
          Arr.splice(x);
        }
      }

    }
    this.filters[filterNom] = Arr.join(',');
    console.log('filter ' + filterNom + ': ' + this.filters[filterNom]);
    this.toApplyFilters = true;
  }
  inFilter(filterNom, value): boolean {
    const z_filter = this.filters[filterNom];
    const Arr =
      (z_filter != undefined &&
        z_filter != ''
        && z_filter[filterNom] != undefined)
        ? z_filter[filterNom].split(',')
        : [];
    for (const x in Arr) {
      if (Arr[x] == value) {
        return true;
      }
    }
    return false;
  }


  fetchProductFilters() {
    this._svc.getProductFilters(this.categoryID, this.selectedType).subscribe((res) => {
      const results = res;

      delete results['status'];

      this.filterControls = { ...this.filterControls, ...results };
    });

  }
  doLoading() {
    if (!this.loadingDialog) {
      this.loadingDialog = this.dialog.open(LoadingModalComponent, {
        width: '250px',
        data: {}
      });
    }


  }
  stopLoading() {
    if (this.loadingDialog != false) {
      this.loadingDialog.close();
      this.loadingDialog = false;
    }

  }


  fetchFilteredProducts(offset?: number, limit?: number, disableLoading?: boolean) {
    if (!disableLoading) {
      this.doLoading();
    }

    this.toApplyFilters = false;
    this.isfiltered = true;
    offset = !offset ? 0 : offset;
    limit = !limit ? 10 : limit;
    this._svc.getProducts(this.categoryID, 'filtered', this.filters, offset, limit).subscribe((res) => {


      if (offset == 0) {

        // tslint:disable: no-string-literal
        this.products = res['result'];
        this.stopLoading();
        // console.log(this.products);
      } else {

        if (!this.isMoreProd) {
          return null;
        }
        this.isMoreProd = false;
        if (res['result'] != null) {

          this.products = [...this.products, ...res['result']];
          this.isMoreProd = true;
        } else {
          this.isMoreProd = false;
        }
        this.stopLoading();

      }
    });
  }
  getTypes(catID) {
    if (this.categoryInfo != undefined) {
      for (const tuple of this.categoryInfo) {
        if (tuple.id == catID) {
          // tslint:disable-next-line: no-string-literal
          return tuple['types'];
        }
      }
    }
  }
  changeProductType() {
    // reset filters
    this.filters = { sort: this.sortField, typeid: this.selectedType };
    this.fetchProductFilters();
    this.fetchFilteredProducts();
  }
  changeProductTypeById(selTypeid) {

    this.selectedType = selTypeid;
    // reset filters
    this.filters = { sort: this.sortField, typeid: selTypeid };
    this.fetchProductFilters();
    this.fetchFilteredProducts();
  }
  openProdDetails(productobj) {
    console.log(productobj);
    this.currentProd = productobj;
    this.prodId = productobj.id;
  }
  closePrdoDetails() {
    this.prodId = false;
  }

  changeSortField() {
    // tslint:disable-next-line: no-string-literal
    this.filters['sort'] = this.sortField;
    this.fetchFilteredProducts();
  }
  /* -----------------------Wishlist Section----------------------- */

  /**
   * adds product to wishlist
   * @param productobj Product object with all details
   */
  addToWishList(productobj) {
    this.wishlisted = 'transit';
    this.wishlistFlags['' + productobj.id] = 'transit';
    const uid = this.checkLogin();
    const wishlistID = this.inWishlist(productobj);
    console.log(wishlistID);
    if (uid) {
      if (!wishlistID) {
        this._svc.addToWishlist(productobj.id, uid).subscribe((res) => {
          this._snackBar.open('Added To Wishlist', null, {
            duration: 2000,
          });
          this.wishlisted = 'yes';
          this.wishlistFlags['' + productobj.id] = 'yes';
          this.refreshWishlist();
        },
          (err) => {
            this._snackBar.open('There was an error.', null, {
              duration: 2000,
            });
            this.wishlistFlags['' + productobj.id] = 'no';
            this.refreshWishlist();
          }
        );

      } else {
        this._svc.removeFromWishlist(wishlistID).subscribe((res) => {
          this._snackBar.open('Removed from Wishlist', null, {
            duration: 2000,
          });
          this.wishlistFlags[productobj.id] = 'no';
          this.refreshWishlist(true);
        });

      }
    }
  }

  /**
   * Fetches userwishlist from api
   * @param clearFlags indicate whether existing flags have to be cleared
   * @returns void
   */
  refreshWishlist(clearFlags?: boolean) {
    // const uidWishlist = this._svc.fetchUserWishlist(this._fauth.getCurrentUID());
    const uidWishlist = this._svc.fetchUserWishlist(this._user.info('id'));
    uidWishlist.toPromise().then((res) => {
      // tslint:disable-next-line: no-string-literal
      this.userWishlist = res['result'];
      if (clearFlags) {
        this.wishlistFlags = [];
      }
      if (this.userWishlist) {
        for (const item of this.userWishlist) {
          this.wishlistFlags['' + item.id] = 'yes';
        }
      }
    });

  }

  /**
   * Returns wishlist entry ID if provided product is in user wishlist, returns false if not in wishlist
   * @param productobj Product object with all product details
   * @returns boolean
   */
  inWishlist(productobj) {

    if (this.userWishlist) {

      for (const item of this.userWishlist) {
        if (item.id == productobj.id) {
          // console.log('In wishlist.');
          // console.log(productobj);
          return item['wishlistid'];
        }
      }
      return false;
    } else {
      return false;
    }
  }
  /* --------------Cart Section------------------- */
  /**
   * Adds product to cart
   * @param productobj Product object containing all product info
   */
  addToCart(productobj) {
    const uid = this.checkLogin();
    if (uid) {
      this._svc.addToCart(productobj.product.id, productobj.quantity, productobj.priceid, uid).subscribe((res) => {
        this._snackBar.open('Added To Cart', null, {
          duration: 2000,
        });
      });
    }
  }
  addDefaultToCart(productobj) {
    const uid = this.checkLogin();
    if (uid) {
      this._svc.addToCart(productobj.id, 1, productobj.prices[0].id, uid).subscribe((res) => {
        this._snackBar.open('Added To Cart', null, {
          duration: 2000,
        });
      });
    }
  }
  fetchMore() {
    if (!this.isMoreProd) {
      return null;
    }

    if (this.isfiltered) {
      this.fetchFilteredProducts(this.products.length + 1, 10, true);

    } else {
      this.isMoreProd = false;
      this._svc.getProducts(this.categoryID, 'items', false, this.products.length + 1, 10)
        .subscribe((resp) => {
          if (resp['result'] != null) {
            this.products = [...this.products, ...resp['result']];
            this.isMoreProd = true;
          } else {
            this.isMoreProd = false;
          }

        });

    }

  }
  loopable(stuff) {
    let truthy = stuff ? true : false;
    if (stuff == undefined) {
      truthy = false;
    }
    if (stuff == null) {
      truthy = false;
    }
    if (stuff == {}) {
      truthy = false;
    }
    if (stuff == []) {
      truthy = false;
    }
    if (truthy) {
      if (stuff.length == 0) {
        truthy = false;
      }
    }
    return truthy;
  }
  validProduct(product) {
    let truthy = product ? true : false;
    if (product == undefined) {
      truthy = false;
    }
    if (product == null) {
      truthy = false;
    }
    if (product == {}) {
      truthy = false;
    }
    if (product == []) {
      truthy = false;
    }
    if (truthy) {
      if (product.length == 0) {
        truthy = false;
      }
    }
    return truthy;
  }
  validPrice(price) {
    let truthy = price ? true : false;
    if (price == undefined) {
      truthy = false;
    }
    if (price == null) {
      truthy = false;
    }
    if (price == {}) {
      truthy = false;
    }
    if (price == []) {
      truthy = false;
    }
    if (truthy) {
      if (price.length == 0) {
        truthy = false;
      }
    }
    return truthy;
  }
  checkLogin() {
    if (!this._fauth.getCurrentUID()) {
      this.router.navigate(['/user/login']);
      return false;
    } else {
      return this._user.info('id');
    }

  }
}
