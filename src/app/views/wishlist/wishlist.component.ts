import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { TovickbrowserService } from 'src/app/services/tovickBrowser/tovickbrowser.service';
import { FauthService } from 'src/app/services/fauth/fauth.service';


@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.scss']
})
export class WishlistComponent implements OnInit {
  items: any;
  image_path: string = environment.image_host + environment.image_subpath.product;

  constructor(
    private _svc: TovickbrowserService,
    private _fauth: FauthService,
    public route: Router
  ) { }

  ngOnInit() {
    this.fetch_wishlist();

  }
  removeFrmList(wid) {
    this._svc.removeFromWishlist(wid).subscribe((res) => {
      this.fetch_wishlist();
    });
  }
  fetch_wishlist() {
    const uid = this._fauth.getCurrentUID();
    if (uid) {
      this._svc.fetchUserWishlist("\'"+uid+"\'").subscribe((res) => {
        // tslint:disable-next-line: no-string-literal
        this.items = res['result'];
      });
    } else {
      this.route.navigate(['/user/login']);
    }
  }

}
