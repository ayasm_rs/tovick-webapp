import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class FauthService {
	// tslint:disable: indent
	private userInfo;
	private phoneConf;
	private fbase_user;

	constructor(private router: Router, private http: HttpClient) {
		if (!firebase.apps.length) {
			firebase.initializeApp(environment.firebase);
		}
		// firebase.auth.useDeviceLanguage();


	}

    /**
     * Returns new instance of firebase
     * @returns { <any>}
     */

	getAuthInstance() {
		return firebase.auth;
	}

    /**
     * Returns a recaptcha verifier
     * @param {callbackfn} callback function to handle captcha success
     * @returns { Promise<any>}
     */
	getVerifier(referrerID, size = 'invisible', callbackfn = false, expired_callback = false) {
		console.log('recaptcha container:' + referrerID);
		return new firebase.auth.RecaptchaVerifier(referrerID, {
			size,
			callback: callbackfn,
			'expired-callback': expired_callback
		});
	}
    /**
     * Resets recatcha of provided verifier object
     * @param {verifier} reCaptcha verifier
     * @returns { Promise<any>}
     */
	resetVerifier(verifier) {
		verifier.render().then(function (widgetId) {
			verifier.grecaptcha.reset(widgetId);
		});
	}
    /**
     * Sends OTP to user for phone login
     * @param {number} number to be used to authenticate
     * @param  {windowref}reCaptcha verifier
     *  @returns { Promise<any>}
     */
	send_OTP(number, windowref) {
		return firebase.auth().signInWithPhoneNumber(number, windowref.recaptchaVerifier);
	}

    /**
     * Confirms OTP for phone verification
     * @param {code} OTP code to be verified
     * @returns { boolean}
     */
	confirm_OTP(code) {
		if (this.phoneConf === undefined) {
			// throw error because OTP not sent
		} else {
			this.fbase_user.credential = firebase.auth.PhoneAuthProvider.credential(this.phoneConf.verificationId, code);
			firebase.auth().signInAndRetrieveDataWithCredential(this.fbase_user.credential);
		}

	}


	register_w_Email(user) {

		return firebase.auth().createUserWithEmailAndPassword(user.email, user.password).catch(
			function (error) {
				// Handle Errors here.
				const errorCode = error.code;
				const errorMessage = error.message;
				if (errorCode == 'auth/weak-password') {
					alert('The password is too weak.');
				} else {
					alert(errorMessage);
				}
				console.log(error);
			});
	}

    /**
     * Logs in to firebase with custom token from api.
     * @param {token} custom unique token from custom auth server
     * @returns { Promise<any>}
     */
	tokenLogin(token) {
		return firebase.auth().signInWithCustomToken(token).catch(
			function (error) {
				// Handle Errors here.
				const errorCode = error.code;
				const errorMessage = error.message;
				if (errorCode === 'auth/invalid-custom-token') {
					alert('The token you provided is not valid.');
				} else {
					console.error(error);
				}
			});
	}

    /**
     * Stores user data in memory to be pushed to firebase DB
     * @param {payload} user information
     * @returns {void}
     */
	cacheUserInfo(payload) {

		this.userInfo = payload;

	}

    /**
     * Sends user data to firebase DB on login state change
     * @returns {void}
    */
	updateUserData() {
		let userId;
		userId = this.getCurrentUID();


		firebase.auth().onAuthStateChanged(function (user) {
			let dbase;

			let payload;
			dbase = firebase.database();

			payload = JSON.parse(localStorage.getItem('Token')).userInfo;
			console.log(payload);

			dbase.ref('users/' + userId).set(payload)
				.then(function () {
					console.log('Synchronization succeeded');

				})
				.catch(function (error) {
					console.log('Synchronization failed:' + error.message);
					this.signout();
				});




		});



	}

/**
 * Provides storage access to user
 * @param {userUID}  Firebase User UID of the user to be give the storage access to.
 *
 */
	allowStorageAccessTo(userUID) {
		let url;
		let data;
		let headers;
		let options;

		firebase.auth().onAuthStateChanged(function (user) {
			console.log('requesting storage access for user:' + userUID);
			data = { uid: userUID };
			url = 'http://localhost:3000/storageAccess';
			headers = new HttpHeaders({
				'Content-Type': 'application/x-www-form-urlencoded'
			});
			options = { headers };
			this.http.post(url, data, options)
				.subscribe((response: any) => {
					console.log('storage request status:');
					console.log(response);
				});
		});
	}

    /**
     * Returns UID of current user
     * @returns {string}
     */
	getCurrentUID() {
		if (typeof firebase.auth == 'function') {
			if (firebase.auth().currentUser) {
				return firebase.auth().currentUser.uid;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}


    /**
     * logs out current user
     * @returns {void}
     */
	signout() {
		firebase.auth().signOut();
	}

}
