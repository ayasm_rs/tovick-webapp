import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import * as hthelpers from 'src/app/helpers/hthelpers';
import Dexie from 'dexie';

@Injectable({
  providedIn: 'root'
})
export class TovickbrowserService {

  url = environment.service_api;
  private options = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'x-api-key': 'TOVICK1007'
    })
  };
  categories: any = false;
  fetched_products: any = false;
  private db = new Dexie('TovickBrowser');
  private tbl_prods;

  constructor(private http: HttpClient) {
    this.db.version(1).stores({
      products: 'id'
    });
    this.tbl_prods = this.db.table('products');

  }

  getHomeData() {
    const url = this.url + 'Home/loadInfo';
    const options = this.options;
    return this.http.get(url, options);
  }

  fetchCategories() {
    const url = this.url + 'ProductCategory/listNodes';
    const options = this.options;
    const subscription = this.http.get(url, options);
    subscription.subscribe((res) => {
      // tslint:disable-next-line: no-string-literal
      this.categories = res['result'];
    });
    return subscription;

  }
  getCatSiblings(id) {
    // console.log('Searching: ' + id);
    if (!this.categories) {
      // console.log('nocategories');
      this.fetchCategories();
      return false;
    }
    // tslint:disable-next-line: prefer-const
    for (let category of this.categories) {
      // console.log(category);
      // tslint:disable-next-line: prefer-const
      for (let subcategory of category.category) {
        // console.log('Subcategory');
        // console.log(subcategory.id);
        // tslint:disable-next-line: triple-equals
        if (subcategory.id == id) {
          // console.log('Sub category Found!');
          return category.category;
        }
      }

    }
    return null;

  }
  getProducts(categoryId: number, mode?: string, filters?: any, offset?: number, limit?: number) {

    offset = !offset ? 0 : offset;
    limit = !limit ? 10 : limit;
    mode = !mode ? 'items' : mode;
    const url = mode === 'filtered' ? (this.url + 'Product/listbyfilter') : (this.url + 'Product/listItems');
    const data = {
      categoryid: categoryId,
      limit,
      offset,
      user_id: 6
    };
    if (mode === 'filtered') {
      Object.assign(data, filters);

    }

    const options: any = this.options;
    options.params = data;
    const subscription = this.http.get(url, options);
    subscription.subscribe((res) => {
      // tslint:disable-next-line: no-string-literal
      this.fetched_products = res['result'];
      if (this.fetched_products != null) {
        this.tbl_prods.bulkPut(this.fetched_products);
      }

    });
    return subscription;

  }
  /**
   * Reads IndexedDB for product information returns data if found.
   */
  getStoredProduct(id) {
    return this.tbl_prods.where('id').equals('' + id).toArray();

  }

  getProductFilters(catID, typeID) {
    const data = {
      categoryid: catID,
      typeid: typeID
    };
    const options: any = this.options;
    options.params = data;
    const url = this.url + 'Product/productUtil';

    const subscription = this.http.get(url, options);
    return subscription;

  }

  /*
  * wishlist
  */
  fetchUserWishlist(userID) {
    const data = {
      id: userID,
    };
    const options: any = this.options;
    options.params = data;
    const url = this.url + 'Product/getwishlist';

    const subscription = this.http.get(url, options);
    return subscription;



  }
  addToWishlist(productId, userId) {
    const data = {
      product_id: productId,
      user_id: userId
    };
    const options: any = {
      headers: new HttpHeaders({
        'x-api-key': 'TOVICK1007'
      })
    };
    options.params = {};
    const fdata = hthelpers.toFormData(data);


    const url = this.url + 'Product/addwishlist';

    const subscription = this.http.post(url, fdata, options);
    return subscription;

  }
  removeFromWishlist(wishlistID) {
    const data = {
      id: wishlistID,
    };
    const options: any = this.options;
    options.params = data;
    const url = this.url + 'Product/removewishlist';


    const subscription = this.http.get(url, options);
    return subscription;

  }

  /*
  * cart
  */
  fetchUserCart(uid) {
    const data = {
      user_id: uid,
      offset: 0,
      limit: 10,
    };
    const options: any = this.options;
    options.params = data;
    const url = this.url + 'Cart/listItems';

    const subscription = this.http.get(url, options);
    return subscription;

  }
  addToCart(productId, quantity, priceId, userId) {
    const data = {
      quantity,
      user_id: userId,
      price_id: priceId,
      product_id: productId,
    };
    const options: any = {
      headers: new HttpHeaders({
        'x-api-key': 'TOVICK1007'
      })
    };
    options.params = {};
    const fdata = hthelpers.toFormData(data);

    const url = this.url + 'Cart/updateCart';


    const subscription = this.http.post(url, fdata, options);
    return subscription;
  }
  addComboToCart(uid: any, offerId: number, product_ids, price, mappingobj) {
    const productIdList = product_ids.join();
    // const mapping=' [{cat_code:first,product_id:87},{cat_code:second,product_id:90}]';
    const mapping = JSON.stringify(mappingobj);

    const data = {
      quantity: '1',
      user_id: uid,
      offer_id: offerId,
      product_ids_exist: product_ids[0],
      product_ids: productIdList,
      product_count: product_ids.length,
      price,
      product_mapping: mapping
    };
    const options: any = {
      headers: new HttpHeaders({
        'x-api-key': 'TOVICK1007'
      })
    };
    options.params = {};
    const fdata = hthelpers.toFormData(data);
    console.log('' + fdata);
    const url = this.url + '/Cart/updateCartCombo';
    const subscription = this.http.post(url, fdata, options);
    return subscription;

  }
  removeFromCart(cartId) {
    const data = {
      id: cartId,
    };
    const options: any = this.options;
    options.params = data;
    const url = this.url + 'Cart/deleteCartItem';


    const subscription = this.http.get(url, options);
    return subscription;


  }
  removeComboFromCart(cartId) {
    const data = {
      id: cartId,
    };
    const options: any = this.options;
    options.params = data;
    const url = this.url + 'Cart/deleteCartItemCombo';


    const subscription = this.http.get(url, options);
    return subscription;


  }


}

