import { TestBed } from '@angular/core/testing';

import { TovickbrowserService } from './tovickbrowser.service';

describe('TovickbrowserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TovickbrowserService = TestBed.get(TovickbrowserService);
    expect(service).toBeTruthy();
  });
});
