import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import * as hthelpers from 'src/app/helpers/hthelpers';
import { supportsScrollBehavior } from '@angular/cdk/platform';

export interface CustomerRegInfo {
  name: string;
  password: string;
  location_info: string;
  live_lat: string;
  live_lng: string;
  device_type: string;
  mobile_status: string;
  mail_status: string;
  mail: string;
  phone: string;
  address: string;
  zipcode: string;
  city: string;
  role: string;
  status?: string;
  firebase_id?: string;
  firebase_token?: string;
  referralcode?: string;
}


@Injectable({
  providedIn: 'root'
})
export class TovickuserService {
  url = environment.service_api;
  private options = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'x-api-key': 'TOVICK1007'
    })
  };



  private userInfo = { id: 20 };

  constructor(private http: HttpClient) { }
  login(username, phonenumber) {
    const url = this.url + 'User/login';
    const options = {
      headers: new HttpHeaders({
        'x-api-key': 'TOVICK1007'
      })
    };
    const payload = {
     username,
     password: phonenumber
    };
    const fd = hthelpers.toFormData(payload);
    const subsc = this.http.post(url, fd , options);

    return subsc.toPromise();
  }
  /**
   * Registers user with Tovick
   * @param userdata all required fields for user registration
   */
  register(userdata: CustomerRegInfo) {
    const url = this.url + 'User/addUser';
    const options = {
      headers: new HttpHeaders({
        'x-api-key': 'TOVICK1007'
      })
    };
    const fd = hthelpers.toFormData(userdata);
    const subsc = this.http.post(url, fd , options);
    subsc.subscribe((res) => {
      sessionStorage.setItem('TovickUsr', JSON.stringify(res['result']));
    });

    return subsc.toPromise();
  }
  /**
   * Checks session storage for stored user data and returns is
   */
  getUsrInfo() {
    const usr = sessionStorage.getItem('TovickUsr');
    if (usr) {
      return JSON.parse(usr);
    } else {
      return false;
    }

  }
  updateAddress() {

  }
  fetchAddresses() {
    const url = this.url + 'Order/adressByUserId';
    // tslint:disable-next-line: prefer-const
    let options: any = this.options;
    options.params = { user_id: this.info('id') };
    return this.http.get(url, options);
  }

  info(field) {
    const usrData = this.getUsrInfo();
    if (usrData) {
      this.userInfo = usrData;
    } else {

    }
    return this.userInfo[field];

  }


}
