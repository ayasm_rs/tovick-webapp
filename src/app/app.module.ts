import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  SwiperModule, SwiperConfigInterface,
  SWIPER_CONFIG
} from 'ngx-swiper-wrapper';
import {NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import {
  MatButtonModule,
  MatCardModule,
  MatSidenavModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatRadioModule,
  MatOptionModule,
  MatSelectModule,
  MatToolbarModule,
  MatProgressBarModule,
  MatListModule,
  MatIconModule,
  MatGridListModule,
  MatSnackBarModule,
  MatMenuModule,
  MatStepperModule,
  MatDialogModule
} from '@angular/material';
import {ImageZoomModule} from 'angular2-image-zoom';

import {RouterModule} from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import {NgxImageZoomModule} from 'ngx-image-zoom';
// services
import { RoutePartsService } from './services/route-parts/route-parts.service';
import {TovickbrowserService} from './services/tovickBrowser/tovickbrowser.service';
import {FauthService} from './services/fauth/fauth.service';
import {WindowService} from './services/window/window.service';


import { AppComponent } from './app.component';

import { NavigationMainComponent } from './common/navs/navigation-main/navigation-main.component';
import { FooterComponent } from './common/footer/footer.component';
import { ReviewsComponent } from './common/reviews/reviews.component';
import { CollectionsComponent } from './common/collections/collections.component';
import { HomepageBannerComponent } from './common/sliders/homepage-banner/homepage-banner.component';
import { ComboBannerComponent } from './common/sliders/combo-banner/combo-banner.component';
import { PromotedProductsComponent } from './common/sliders/promoted-products/promoted-products.component';
import { LoginModalComponent } from './common/login-modal/login-modal.component';
import { LoadingModalComponent } from './common/loading-modal/loading-modal.component';

import { HomeComponent } from './views/home/home.component';
import { CategoriesComponent } from './views/categories/categories.component';
import { ProductListComponent } from './views/product-list/product-list.component';
import { AddProductComponent } from './admin/add-product/add-product.component';
import { ProductComponent } from './views/product/product.component';
import { CartComponent } from './views/cart/cart.component';
import { MainComponent } from './views/user/main/main.component';
import { AddressComponent } from './views/user/address/address.component';
import { WishlistComponent } from './views/wishlist/wishlist.component';
import { ComboProductsComponent } from './views/combo-products/combo-products.component';
import { ComboProductsListComponent } from './views/combo-products-list/combo-products-list.component';
import { ComboProductDetailsComponent } from './views/combo-product-details/combo-product-details.component';



const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  observer: true,
  direction: 'horizontal',
  threshold: 50,
  spaceBetween: 5,
  slidesPerView: 1,
  centeredSlides: true
};


@NgModule({
  declarations: [
    AppComponent,
    NavigationMainComponent,
    HomeComponent,
    FooterComponent,
    ReviewsComponent,
    CollectionsComponent,
    HomepageBannerComponent,
    ComboBannerComponent,
    PromotedProductsComponent,
    CategoriesComponent,
    ProductListComponent,
    AddProductComponent,
    ProductComponent,
    CartComponent,
    MainComponent,
    AddressComponent,
    WishlistComponent,
    LoginModalComponent,
    ComboProductsComponent,
    ComboProductsListComponent,
    ComboProductDetailsComponent,
    LoadingModalComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    // frontend modules
    SwiperModule,
    NgbModule,
    NgbDropdownModule,
    FormsModule,
    ReactiveFormsModule,
    SwiperModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatSidenavModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatRadioModule,
    MatSelectModule,
    MatOptionModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule,
    MatGridListModule,
    MatSnackBarModule,
    MatMenuModule,
    MatStepperModule,
    MatDialogModule,
    MatProgressBarModule,
    NgxImageZoomModule,
    ImageZoomModule,
  ],
  entryComponents: [LoginModalComponent, LoadingModalComponent],
  providers: [
    RoutePartsService,
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    },
    TovickbrowserService,
    FauthService,
    WindowService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
