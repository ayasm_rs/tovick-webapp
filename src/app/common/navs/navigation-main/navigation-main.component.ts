import { Component, OnInit } from '@angular/core';
import { TovickbrowserService } from 'src/app/services/tovickBrowser/tovickbrowser.service';
import { FauthService } from 'src/app/services/fauth/fauth.service';
import {LoginModalComponent} from 'src/app/common/login-modal/login-modal.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Router} from '@angular/router' ;




@Component({
  selector: 'app-navigation-main',
  templateUrl: './navigation-main.component.html',
  styleUrls: ['./navigation-main.component.scss']
})
export class NavigationMainComponent implements OnInit {
  toggleMobMenu = false;
  toggleMobCategoriesMenu = true;
  categoryInfo: any;
  _auth: any;


  constructor(
    private _svc: TovickbrowserService,
    private _fauth: FauthService,
    public dialog: MatDialog,
    public route: Router,
    ) { }

  ngOnInit() {
    this._auth = this._fauth.getAuthInstance();
    this._svc.fetchCategories().subscribe((res) => {
      // tslint:disable-next-line: no-string-literal
      this.categoryInfo = res['result'];
    });
  }
  closenavs() {
    this.toggleMobCategoriesMenu = false;
    this.toggleMobMenu = false;
  }
  opennavs() {
    this.toggleMobCategoriesMenu = true;
    this.toggleMobMenu = true;
  }
  isLogin() {
    const uid = this._fauth.getCurrentUID();
    return uid;
  }
  loginStuff() {
    const dialogRef = this.dialog.open(LoginModalComponent, {
      width: '400px',
      data: {test: 'Hello'}
    });


  }
  logout(){
    this._fauth.signout();
  }
  toUserAc() {
    this.route.navigate(['/user']);
  }


}
