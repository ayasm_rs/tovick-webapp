import {Component, OnInit, Inject} from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
// services
import { FauthService } from 'src/app/services/fauth/fauth.service';
import { WindowService } from 'src/app/services/window/window.service';
import { CustomerRegInfo, TovickuserService } from 'src/app/services/tovickuser.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})
export class LoginModalComponent implements OnInit {

  inputForm: FormGroup;
  otpVerificationForm: FormGroup;
  windowRef: any;
  confirmation: any;

  formHeading = 'Login';
  formMsg = 'Get access to your Orders and Wishlist';
  stage = 'input';
  error = false;
  processing = false;
  private authObject;

  constructor(
    public dialogRef: MatDialogRef<LoginModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _fauth: FauthService,
    private _win: WindowService,
    private _tUser: TovickuserService,
    public route: Router,
  ) { }

  ngOnInit() {

    this.checkLogin();
    this.authObject = this._fauth.getAuthInstance();
    console.log(this.authObject);

    this.windowRef = this._win.windowRef;
    // Initialize login form
    this.inputForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      mail: new FormControl('', Validators.compose([Validators.required, Validators.email])),
      mobile: new FormControl('', Validators.compose([Validators.required/*, Validators.pattern('^[2-9]{2}[0-9]{8}$')*/]))
    });

    // Initialize OTP entry form
    this.otpVerificationForm = new FormGroup({
      otp: new FormControl('', Validators.required)
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  checkLogin() {
    if (this._fauth.getCurrentUID()) {
      this.route.navigate(['/wishlist']);
      return true;
    } else {
      return false;
    }
  }
  setCaptcha(switcher = true, winRef?) {
    winRef = !winRef ? this.windowRef : winRef;
    const truthy: boolean = winRef.recaptchaVerifier == undefined ? true : false;
    if (switcher) {
      // Initialize recaptcha on container
      const myauth = this._fauth.getAuthInstance();
      if (myauth == undefined) {
        console.log('Auth not found');
        console.log(myauth);
      }
      winRef.recaptchaVerifier = new this.authObject.RecaptchaVerifier('recaptcha-container', { size: 'invisible' });
      winRef.recaptchaVerifier.render();
    } else {
      // reset captcha
      console.log('Resetting the Captcha\n');
      winRef.recaptchaVerifier.render().then((widgetId) => {
        winRef.grecaptcha.reset(widgetId);
      });
    }
  }
  sendCode() {
    if (this.checkLogin()) {
      return null;
    }
    this.processing = true;
    this.setCaptcha(!this.error, this.windowRef);
    // call to mobile API here
    const data = this.inputForm.value;
    this._fauth.send_OTP('+91' + data.mobile, this.windowRef).then(
      result => {
        console.log('SMS Sent:');
        console.log(result);
        this.formHeading = 'OTP';
        this.formMsg = 'Enter the verification code sent to your phone to continue';
        this.stage = 'otp';
        this.confirmation = result;
        this.processing = false;
      })
      .catch(
        error => {
          console.log(error);
          this.formHeading = 'ERROR';
          this.formMsg = 'Try again.';
          this.error = true;
          // reset captcha
          this.setCaptcha(false, this.windowRef);
          this.processing = false;
        });
  }
  verify() {
    this.processing = true;
    const otp = this.otpVerificationForm.value.otp;
    // verify otp
    this.confirmation.confirm(otp).then(result => {
      this.userRegistration().then((res) => {
        this.onNoClick();
      });
      // alert('OTP Verified.');
      // localStorage.setItem('phone', JSON.stringify(this.numVerificationForm.value.mnumber));
      // this.route.navigate(['/']);
      this.processing = false;
    })
      .catch(error => {
        console.log(error.message);
        this.error = true;
        // show signin form
        this.formHeading = 'Error';
        this.formMsg = 'Invalid OTP. Try again.';
        this.stage = 'input';
        this.processing = false;

      });

  }
  userRegistration() {
    const UID = this._fauth.getCurrentUID();
    const regData: CustomerRegInfo = {
      device_type: 'WEB',
      live_lat: '0.0',
      live_lng: '0.0',
      location_info: '0.0',
      mail: this.inputForm.get('mail').value,
      mail_status: 'UNVERIFIED',//set to verified only if mail login done via firebase
      phone: this.inputForm.get('mobile').value,
      mobile_status: 'VERIFIED',
      firebase_id: UID ? UID : '',
      name: this.inputForm.get('mobile').value,
      password: this.inputForm.get('mobile').value,
      address: '',
      city: '',
      zipcode:'',
      role: 'USER',

    };

    return this._tUser.register(regData);

  }

  toShowButton(stagename): boolean {
    if (this.stage == stagename && !this.processing) {
      return true;
    }
    return false;

  }

}
