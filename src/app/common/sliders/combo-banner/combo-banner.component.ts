import { Component, OnInit, SecurityContext, Input, ViewChild } from '@angular/core';
import {
  SwiperComponent, SwiperDirective, SwiperConfigInterface,
  SwiperScrollbarInterface, SwiperPaginationInterface
} from 'ngx-swiper-wrapper';
import { DomSanitizer, SafeHtml, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-combo-banner',
  templateUrl: './combo-banner.component.html',
  styleUrls: ['./combo-banner.component.scss']
})
export class ComboBannerComponent implements OnInit {
  imagepath = environment.image_host + 'combo/';
  swiperConfig: SwiperConfigInterface = {
    slidesPerView: 4,
    spaceBetween: 30,
    centeredSlides: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
  };
  @ViewChild(SwiperDirective) directiveRef?: SwiperDirective;
 @Input() slides = [
    {
      offer: {
        id: '1',
        name: '',
        image_path: 'offer_2.jpg',
        description: '',
        offer_type: 'COMBO_GIFTS',
        offer_cat_one: '34',
        offer_one_max_price: '399',
        offer_cat_two: '34',
        offer_two_max_price: '399',
        offer_cat_three: '0',
        offer_three_max_price: '0',
        offer_price: '599',
        offer_percentage_upto: '40',
        offer_one_name: 'a Coffee Mug of your Choice',
        offer_two_name: 'a Coffee Mug of your Choice',
        offer_three_name: '',
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 'ACTIVE'
      },
      off_cat_one_image: 'ic_subcategory.png',
      off_cat_two_image: 'ic_subcategory.png',
      off_cat_three_image: '',
      off_cat_one_name: 'a Coffee Mug of your Choice',
      off_cat_one_types: null,
      off_cat_two_name: 'a Coffee Mug of your Choice',
      off_cat_two_types: null,
      off_cat_three_name: '',
      off_cat_three_types: null
    },
    {
      offer: {
        id: '2',
        name: '',
        image_path: 'offer_3.jpg',
        description: '',
        offer_type: 'COMBO_GIFTS',
        offer_cat_one: '15',
        offer_one_max_price: '399',
        offer_cat_two: '15',
        offer_two_max_price: '399',
        offer_cat_three: '15',
        offer_three_max_price: '399',
        offer_price: '799',
        offer_percentage_upto: '60',
        offer_one_name: 'a Printed Cushion Cover of your Choice',
        offer_two_name: 'a Printed Cushion Cover of your Choice',
        offer_three_name: 'a Printed Cushion Cover of your Choice',
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 'ACTIVE'
      },
      off_cat_one_image: 'ic_subcategory.png',
      off_cat_two_image: 'ic_subcategory.png',
      off_cat_three_image: 'ic_subcategory.png',
      off_cat_one_name: 'a Printed Cushion Cover of your Choice',
      off_cat_one_types: null,
      off_cat_two_name: 'a Printed Cushion Cover of your Choice',
      off_cat_two_types: null,
      off_cat_three_name: 'a Printed Cushion Cover of your Choice',
      off_cat_three_types: null
    },
    {
      offer: {
        id: '3',
        name: '',
        image_path: 'offer_4.jpg',
        description: '',
        offer_type: 'COMBO_GIFTS',
        offer_cat_one: '15',
        offer_one_max_price: '399',
        offer_cat_two: '40',
        offer_two_max_price: '399',
        offer_cat_three: '34',
        offer_three_max_price: '399',
        offer_price: '699',
        offer_percentage_upto: '60',
        offer_one_name: 'a Printed Cushion Cover of your Choice',
        offer_two_name: 'Single Photo frame of your choice',
        offer_three_name: 'a Coffee Mug of your Choice',
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 'ACTIVE'
      },
      off_cat_one_image: 'ic_subcategory.png',
      off_cat_two_image: 'ic_subcategory.png',
      off_cat_three_image: 'ic_subcategory.png',
      off_cat_one_name: 'a Printed Cushion Cover of your Choice',
      off_cat_one_types: null,
      off_cat_two_name: 'Single Photo frame of your choice',
      off_cat_two_types: null,
      off_cat_three_name: 'a Coffee Mug of your Choice',
      off_cat_three_types: null
    },
    {
      offer: {
        id: '4',
        name: '',
        image_path: 'offer_5.jpg',
        description: '',
        offer_type: 'COMBO_GIFTS',
        offer_cat_one: '40',
        offer_one_max_price: '399',
        offer_cat_two: '40',
        offer_two_max_price: '399',
        offer_cat_three: '0',
        offer_three_max_price: '0',
        offer_price: '499',
        offer_percentage_upto: '50',
        offer_one_name: 'Single Photo frame of your choice',
        offer_two_name: 'Single Photo frame of your choice',
        offer_three_name: '',
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 'ACTIVE'
      },
      off_cat_one_image: 'ic_subcategory.png',
      off_cat_two_image: 'ic_subcategory.png',
      off_cat_three_image: '',
      off_cat_one_name: 'Single Photo frame of your choice',
      off_cat_one_types: null,
      off_cat_two_name: 'Single Photo frame of your choice',
      off_cat_two_types: null,
      off_cat_three_name: '',
      off_cat_three_types: null
    }
  ];

  constructor(private _sanitizer: DomSanitizer) { }

  ngOnInit() {
    if (window.innerWidth <= 480) {
      this.swiperConfig.slidesPerView = 2;
    }

  }

  sanitize_img_url(url: string): string {
    const urlStr = this._sanitizer.sanitize(SecurityContext.URL, url);
    return urlStr;
  }
  get_slide_bg_style(url: string) {
    const styleStr = '{background-image: url("' + this.sanitize_img_url(url) + '");}';
    const safe_style = this._sanitizer.bypassSecurityTrustStyle(styleStr);
    const styleStr_sanitized = this._sanitizer.sanitize(SecurityContext.STYLE, styleStr);
    return safe_style.toString();

  }
  next(){
    this.directiveRef.nextSlide();
  }
  prev(){
    this.directiveRef.prevSlide();
  }

}
