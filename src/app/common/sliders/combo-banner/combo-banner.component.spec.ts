import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComboBannerComponent } from './combo-banner.component';

describe('ComboBannerComponent', () => {
  let component: ComboBannerComponent;
  let fixture: ComponentFixture<ComboBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComboBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComboBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
