import { Component, OnInit, SecurityContext, Input, ViewChild } from '@angular/core';
import {
  SwiperComponent, SwiperDirective, SwiperConfigInterface,
  SwiperScrollbarInterface, SwiperPaginationInterface
} from 'ngx-swiper-wrapper';
import { DomSanitizer, SafeHtml, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-homepage-banner',
  templateUrl: './homepage-banner.component.html',
  styleUrls: ['./homepage-banner.component.scss']
})
export class HomepageBannerComponent implements OnInit {
  imagepath = environment.image_host + 'slider/';
  progressbar: SwiperPaginationInterface ;

  swConfigObj: SwiperConfigInterface = {
    grabCursor: true,
    parallax: true,
    loop: true,
    speed: 800,
    slidesPerView: 1,
    spaceBetween: 15,
    centeredSlides: true,
    autoplay: {
      delay: 2000,
      disableOnInteraction: false,
    },
  };
  @ViewChild(SwiperDirective) directiveRef?: SwiperDirective;

 @Input() slides = [
    {
      id: '1',
      name: '0',
      image_path: 'Banner_Frames.jpg',
      types: [
        {
          id: '41',
          name: 'Two Side',
          image_path: 'ic_subcategory.png',
          status: 'ACTIVE',
          category_id: '10',
          position: '2',
          tax: '5'
        },
        {
          id: '40',
          name: 'Single',
          image_path: 'ic_subcategory.png',
          status: 'ACTIVE',
          category_id: '10',
          position: '1',
          tax: '5'
        }
      ],
      category_id: '10',
      page_path: '',
      category_name: 'Photo Frames & Album'
    },
    {
      id: '2',
      name: '0',
      image_path: 'Banner_light.jpg',
      types: [
        {
          id: '37',
          name: 'T Light Holders',
          image_path: 'ic_subcategory.png',
          status: 'ACTIVE',
          category_id: '25',
          position: '0',
          tax: '12'
        },
        {
          id: '38',
          name: 'Lanterns',
          image_path: 'ic_subcategory.png',
          status: 'ACTIVE',
          category_id: '25',
          position: '0',
          tax: '12'
        },
        {
          id: '39',
          name: 'Decorative Lamps',
          image_path: 'ic_subcategory.png',
          status: 'ACTIVE',
          category_id: '25',
          position: '0',
          tax: '12'
        }
      ],
      category_id: '25',
      page_path: '',
      category_name: 'Lighting & Lamps'
    },
    {
      id: '3',
      name: '0',
      image_path: 'Banner_Mug.jpg',
      types: [
        {
          id: '34',
          name: 'Coffee Mug',
          image_path: 'ic_subcategory.png',
          status: 'ACTIVE',
          category_id: '13',
          position: '0',
          tax: '12'
        }
      ],
      category_id: '13',
      page_path: '',
      category_name: 'Mugs'
    }
  ];


  swiperConfig: SwiperConfigInterface;

  constructor(private _sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.swiperConfig = this.swConfigObj;
  }

  sanitize_img_url(url: string): string {
    const urlStr = this._sanitizer.sanitize(SecurityContext.URL, url);

    return urlStr;
  }
  get_slide_bg_style(url: string) {
    const styleStr = '{background-image: url("' + this.sanitize_img_url(url) + '");}';

    const safe_style = this._sanitizer.bypassSecurityTrustStyle(styleStr);
    const styleStr_sanitized = this._sanitizer.sanitize(SecurityContext.STYLE, styleStr);
    return safe_style.toString();

  }
  next(){
    this.directiveRef.nextSlide();
  }
  prev(){
    this.directiveRef.prevSlide();
  }

}
