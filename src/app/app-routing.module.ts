import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './views/home/home.component';
import {ProductListComponent} from './views/product-list/product-list.component';
import {CategoriesComponent} from './views/categories/categories.component';
import {ProductComponent} from './views/product/product.component';
import {CartComponent} from './views/cart/cart.component';
import {MainComponent} from './views/user/main/main.component';
import {WishlistComponent} from './views/wishlist/wishlist.component';
import { ComboProductsComponent } from './views/combo-products/combo-products.component';

const routes: Routes = [

  {
    path: '',
    component: HomeComponent,
    data: {title: 'Welcome to homepage'}
  },
  {
    path: 'products/:cid',
    component: ProductListComponent,
    data: {title: 'Showing'}
  },
  {
    path: 'combo',
    component: ComboProductsComponent,
    data: {title: 'Combo'}
  },
  {
    path: 'combo/:cid',
    component: ComboProductsComponent,
    data: {title: 'Combo'}
  },

  {
    path: 'products/:cid/:pid',
    component: ProductListComponent,
    data: {title: 'Showing'}
  },
  {
    path: 'product',
    component: ProductComponent,
    data: {title: 'Product'}
  },
  {
    path: 'cart',
    component: CartComponent,
    data: {title: 'Cart'}
  },
  {
    path: 'wishlist',
    component: WishlistComponent,
    data: {title: 'Wishlist'}
  },
  {
    path: 'categories',
    component: CategoriesComponent,
    data: {title: 'Product Categories'}
  },
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'user',
        loadChildren: './views/user/user.module#UserModule',
        data: {title: 'Manage Account'}

      }
    ],
    data: {title: 'Change Your Address'}
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
