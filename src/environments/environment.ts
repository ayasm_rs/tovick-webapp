// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  image_host: 'https://tovick.com/backend/images/',
  image_subpath: {
    interest: 'interest/',
    product: 'product/',
    category: 'category/',
    subcategory: 'subcategory/',
    combo: 'combo/',
    util: 'util/',
    event: 'event/',
    slider: 'slider/',
    subscribe: 'subscribe/',

  },
  service_api: 'https://tovick.com/backend/api/',
  firebase: {
    apiKey: 'AIzaSyDGSO4iKG-3D0OXCEMyd9lPxEBG_omK_ws',
      authDomain: 'https://tovick-d0e41.firebaseapp.com',
      storageBucket: 'tovick-d0e41.appspot.com',
      projectId: '433026516800',
   }
  /* firebase: {
    apiKey: "AIzaSyCaXtBV7F8YTlGYWZylnFNIFAS6pcmvn8Q",
      authDomain: "bestprice-e1601.firebaseapp.com",
      storageBucket: "bestprice-e1601.appspot.com",
      projectId: "942539971133",
   } */
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
