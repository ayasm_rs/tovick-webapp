export const environment = {
  production: true,
  image_host: 'https://tovick.com/backend/images/',
  image_subpath: {
    interest: 'interest/',
    product: 'product/',
    category: 'category/',
    subcategory: 'subcategory/',
    combo: 'combo/',
    util: 'util/',
    event: 'event/',
    slider: 'slider/',
    subscribe: 'subscribe/',

  },
  service_api: 'https://tovick.com/backend/api/',
  firebase: {
    apiKey: "AIzaSyDGSO4iKG-3D0OXCEMyd9lPxEBG_omK_ws",
      authDomain: "https://tovick-d0e41.firebaseapp.com",
      storageBucket: "tovick-d0e41.appspot.com",
      projectId: "433026516800",
   }
};
